function problem6(data){
    if (data == undefined){
        return [];
    }
    let ans=[];
    for (let idx=0; idx < data.length; idx++) {
        if(data[idx].car_make=='Audi' || data[idx].car_make=='BMW'){
            ans.push(data[idx]);
        }
    }
    return ans;
}

module.exports=problem6;