function problem5(data){
    if (data ==undefined){
        return [];
    }
    let ans=[];
    for (let idx=0; idx < data.length; idx++) {
        if(data[idx].car_year<2000){
            ans.push(data[idx].car_year);
        }
    }
    return ans;
}

module.exports=problem5;