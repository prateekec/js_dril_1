function problem1(data,n){
    if(typeof(data)!="object" || typeof(n) != "number"){
        // console.log("if_first");
        return [];
    }
    if(data['id'] != undefined){
        if(data.id!=n){
            // console.log("if_second");
            return {};
        }
        if(data.car_make == undefined){
            // console.log("kjdflkj");
            return [];
        }
        return data;
    }
    let ans=[];
    for (let idx=0; idx < data.length; idx++) {
        if(data[idx].id ==n){
            ans=data[idx];
            break;
        }
    }
    //console.log("if_last");
    return ans;
}
module.exports=problem1;


