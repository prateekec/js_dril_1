function problem3(data){
    if(data==undefined){
        return [];
    }
    let ans=[];
    for (let idx=0; idx < data.length; idx++) {
        ans[idx]=data[idx].car_model;
    }
    ans.sort();
    return ans;
}

module.exports=problem3;