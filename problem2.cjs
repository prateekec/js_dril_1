function problem2(data={}){
    if(data.length==undefined){
        console.log("");        
    }
    else{
        let ans=data[data.length-1];
        console.log("Last car is a "+ans.car_make+' '+ans.car_model+'.');
    }
}
module.exports=problem2;
